import { combineReducers } from 'redux';
import loginReducer from './loginReducer.js';
import bookshelf from './bookshelf';

//Importa em cima, declara em baixo

export default combineReducers({ session: loginReducer, bookshelf }); // <- Declare aqui
