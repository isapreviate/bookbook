export const login = (currentToken, user) => ({
  type: 'LOGIN',
  currentToken,
  user,
});
export const addBooks = (books) => ({ type: 'ADD_BOOKS', books });

export const updateUser = (newUser) => ({ type: 'UPDATE_USER', newUser });

export const clearLogin = () => ({ type: 'CLEAR_LOGIN' });
