import React from "react";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { withStyles, createStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { addBooks } from "../../action";
import loadBooksList from "../helpers/loadBooksList";
import Button from "@material-ui/core/Button";

const styles = (theme) =>
  createStyles({
    buttonCancel: {
      color: "ghostwhite",
      backgroundColor: "crimson",
      marginLeft: "4px",
      marginRight: "4px",
    },
    buttonConfirm: {
      color: "ghostwhite",
      backgroundColor: "midnightblue",
      marginLeft: "4px",
      marginRight: "4px",
    },
    textBox: {
      width: "100%",
    },
  });

class EditBookReview extends React.Component {
  state = {
    comment: { value: "", error: "", helperText: "" },
  };

  componentDidMount() {
    this.setState({
      comment: { ...this.state.comment, value: this.props.review },
    });
  }

  updateReview = (e) => {
    fetch(
      `https://ka-users-api.herokuapp.com/users/${this.props.user.id}/books/${this.props.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: this.props.currentToken,
        },
        body: JSON.stringify({
          book: {
            review: this.state.comment.value,
          },
        }),
      }
    ).then(() =>
      loadBooksList(this.props.user.id, this.props.currentToken)
        .then((res) => res.json())
        .then((res) => {
          this.props.addBooks(res);
          this.props.cancelEdit(e);
        })
    );
  };

  setValueComment = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      comment: { ...this.state.comment, value: value },
    });
  };
  validationComment = () => {
    const comment = this.state.comment.value;
    if (comment.length > 560) {
      return "O comentário não deve possuir mais que 560 caracteres";
    } else {
      return "";
    }
  };
  submeter = (e) => {
    e.preventDefault();
    const commentError = this.validationComment();
    this.setState({
      ...this.state,

      comment: {
        value: this.state.comment.value,
        error: commentError === "" ? "" : "true",
        helperText: commentError,
      },
    });
    if (commentError === "") {
      this.updateReview(e);
    }
  };

  render() {
    const { classes } = this.props;
    console.log(this.state);
    if (this.props.isEditing === this.props.id) {
      return (
        <form className={classes.root} autoComplete="off">
          <TextField
            multiline
            variant="outlined"
            className={classes.textBox}
            rows={5}
            label="Escreva aqui o seu comentário:"
            value={this.state.comment.value}
            defaultValue={this.props.review}
            onChange={this.setValueComment}
            helperText={this.state.comment.helperText}
            error={this.state.comment.error}
          />
          <br />
          <br />
          <Button
            className={classes.buttonConfirm}
            variant="contained"
            color="primary"
            onClick={(e) => this.submeter(e)}
          >
            Salvar
          </Button>
          <Button
            className={classes.buttonCancel}
            variant="contained"
            color="secondary"
            onClick={this.props.cancelEdit}
          >
            Cancelar
          </Button>
        </form>
      );
    }

    return (
      <Typography variant="body1" color="textPrimary" component="p">
        {this.props.review}
      </Typography>
    );
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
});

const mapDispatchToProps = (dispatch) => ({
  addBooks: (books) => dispatch(addBooks(books)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(EditBookReview));
