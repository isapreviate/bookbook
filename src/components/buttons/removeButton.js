const removeButton = (userId, bookId, token) => {
  return fetch(
    `https://ka-users-api.herokuapp.com/users/${userId}/books/${bookId}`,
    {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    }
  )
}

export default removeButton;