import React from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {
  TextField,
  FormControlLabel,
  Checkbox,
  Button,
  Container,
  FormControl,
  InputLabel,
  FormHelperText,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";

import Recaptcha from "react-recaptcha";

import Background from "./backgroundDefault.png";

import "./signUp.css";

const defaultState = {
  email: { value: "", error: "", helperText: "" },
  name: { value: "", error: "", helperText: "" },
  lastname: { value: "", error: "", helperText: "" },
  username: { value: "", error: "", helperText: "" },
  password: { value: "", error: "", helperText: "" },
  confirmPassword: { value: "", error: "", helperText: "" },
  about: { value: "", error: "", helperText: "" },
  state: { value: "", myState: "", error: "", helperText: "" },
  privacy: { value: "", error: "", helperText: "" },
  checkbox: false,
  checked: false,
  newUserCreated: false,
};

const countries = [
  { title: "Acre", subtitle: "AC" },
  { title: "Alagoas", subtitle: "AL" },
  { title: "Amapá ", subtitle: "AP" },
  { title: "Amazonas", subtitle: "AM" },
  { title: "Ceará", subtitle: "CE" },
  { title: "Distrito Federal", subtitle: "DF" },
  { title: "Espirito Santo", subtitle: "ES" },
  { title: "Goiás", subtitle: "GO" },
  { title: "Maranhão", subtitle: "MA" },
  { title: "Mato Grosso", subtitle: "MT" },
  { title: "Mato Grosso do Sul", subtitle: "MS" },
  { title: "Minas Gerais", subtitle: "MG" },
  { title: "Pará ", subtitle: "PA" },
  { title: "Paraíba", subtitle: "PB" },
  { title: "Paraná", subtitle: "PR" },
  { title: "Pernambuco", subtitle: "PE" },
  { title: "Rio de Janeiro", subtitle: "RJ" },
  { title: "Rio Grande do Norte", subtitle: "RN" },
  { title: "Rio Grande do Sul", subtitle: "RS" },
  { title: "Rondônia", subtitle: "RO" },
  { title: "Roraima ", subtitle: "RR" },
  { title: "Santa Catarina", subtitle: "SC" },
  { title: "São Paulo", subtitle: "SP" },
  { title: "Sergipe", subtitle: "SE" },
  { title: "Tocantins", subtitle: "TO" },
];

const useStyles = (theme) => ({
  signUpButton: {
    background:
      "linear-gradient(180deg, rgba(18,29,77,1) 0%, rgba(0,0,0,1) 100%)",
    color: "ghostwhite",
    borderColor: "ghostwhite",
  },
});

class SignUp extends React.Component {
  state = defaultState;
  /*=============================================================================================================
................................................NOME..........................................................
===============================================================================================================*/

  setValueName = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      name: { ...this.state.name, value: value },
    });
  };

  validationName = () => {
    const name = this.state.name.value;
    if (name === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else if (name.length < 3) {
      return "O nome não deve possuir menos que 3 caracteres";
    } else if (name.length > 24) {
      return "O nome não deve possuir mais que 24 caracteres";
    } else {
      return "";
    }
  };

  /*=============================================================================================================
..................................................SOBRENOME.....................................................
===============================================================================================================*/

  setValueLastname = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      lastname: { ...this.state.lastname, value: value },
    });
  };

  validationLastname = () => {
    const lastname = this.state.lastname.value;
    if (lastname === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else if (lastname.length < 3) {
      return "O sobrenome não deve possuir menos que 3 caracteres";
    } else if (lastname.length > 32) {
      return "O sobrenome não deve possuir mais que 32 caracteres";
    } else {
      return "";
    }
  };

  /*=============================================================================================================
........................................NOME DE USUÁRIO..........................................................
===============================================================================================================*/

  setValueUsername = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      username: { ...this.state.username, value: value },
    });
  };

  validationUsername = () => {
    const username = this.state.username.value;
    if (username === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else if (username.length < 6) {
      return "O nome de usuário não deve possuir menos que 6 caracteres";
    } else if (username.length > 12) {
      return "O nome de usuário não deve possuir mais que 12 caracteres";
    } else {
      return "";
    }
  };

  /*=============================================================================================================
..............................................E-MAIL...........................................................
===============================================================================================================*/

  setValueEmail = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      email: { ...this.state.email, value: value },
    });
  };

  formatEmail = () => {
    const formatEmail = this.state.email.value;

    return this.setState({
      ...this.state,
      email: {
        ...this.state.email,
        value: formatEmail.toLowerCase(),
      },
    });
  };

  validationEmail = () => {
    const validateEmail = this.state.email.value;

    let name = validateEmail.substring(0, validateEmail.indexOf("@"));
    let domain = validateEmail.substring(
      validateEmail.indexOf("@") + 1,
      validateEmail.length
    );

    if (validateEmail === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else if (
      name.length >= 1 &&
      domain.length >= 3 &&
      name.search("@") === -1 &&
      domain.search("@") === -1 &&
      name.search(" ") === -1 &&
      domain.search(" ") === -1 &&
      domain.search(".") !== -1 &&
      domain.indexOf(".") >= 1 &&
      domain.lastIndexOf(".") < domain.length - 1
    ) {
      return "";
    } else {
      return "E-mail incompleto";
    }
  };

  /*=============================================================================================================
.....................................................SENHA.......................................................
===============================================================================================================*/

  setValuePassword = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      password: { ...this.state.password, value: value },
    });
  };

  validationPassword = () => {
    const password = this.state.password.value;

    const capitalLetter = /[A-Z]/;
    const lowerLetter = /[a-z]/;
    const number = /[0-9]/;
    const specialsCharacters = /[!|@|#|$|%|^|&|*|(|)|-|_]/;

    let countCapitalLetters = 0;
    let countLowerLetters = 0;
    let countNumbers = 0;
    let countSpecialCharacters = 0;

    for (let i = 0; i < password.length; i++) {
      if (capitalLetter.test(password[i])) {
        countCapitalLetters++;
      } else if (lowerLetter.test(password[i])) {
        countLowerLetters++;
      } else if (number.test(password[i])) {
        countNumbers++;
      } else if (specialsCharacters.test(password[i])) {
        countSpecialCharacters++;
      }
    }
    if (password === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else if (password.length > 16) {
      return "A senha não deve possuir mais que 16 caracteres";
    } else if (password.length < 8) {
      return "A senha não deve possuir menos que 8 caracteres";
    } else if (
      countCapitalLetters >= 1 &&
      countLowerLetters >= 1 &&
      (countNumbers >= 1 || countSpecialCharacters >= 1)
    ) {
      return "";
    } else {
      return "Sua senha deve conter letras maiúsculas, minusculas, números e/ou caracteres especiais";
    }
  };

  /*=============================================================================================================
...............................................CONFIRMAR SENHA..................................................
===============================================================================================================*/

  setValueConfirmPassword = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      confirmPassword: { ...this.state.confirmPassword, value: value },
    });
  };

  validationConfirmPassword = () => {
    const password = this.state.password.value;
    const confirmPassword = this.state.confirmPassword.value;

    if (confirmPassword === "") {
      return "Ops! Não esqueça de preencher este campo";
    } else if (password !== confirmPassword) {
      return "As senhas não coincidem";
    } else {
      return "";
    }
  };

  /*=============================================================================================================
......................................................SOBRE...................................................
===============================================================================================================*/

  setValueAbout = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      about: { ...this.state.about, value: value },
    });
  };

  validationAbout = () => {
    const about = this.state.about.value;
    if (about.length > 150) {
      return "Este campo não deve possuir mais que 150 caracteres";
    } else if (about.length < 10) {
      return "Conte algo sobre você";
    } else {
      return "";
    }
  };

  /*=============================================================================================================
.....................................................ESTADO....................................................
===============================================================================================================*/

  setValueState = (e, state) => {
    const myState = state;
    this.setState({
      ...this.state,
      state: {
        ...this.state.state,
        value: myState,
        myState: myState.title,
      },
    });
  };

  validationState = () => {
    const about = this.state.about.value;
    if (about.length < 1) {
      return "Selecione o seu estado";
    } else {
      return "";
    }
  };

  /*=============================================================================================================
...........................................TERMOS DE SERVIÇO..................................................
===============================================================================================================*/
  setValueCheckbox = (event) => {
    this.setState({ ...this.state, checkbox: event.target.checked });
  };

  checkCheckbox = () => {
    if (this.state.checkbox === false) {
      return true;
    }
  };

  validationCheckbox = () => {
    const checkbox = this.state.checkbox;
    if (checkbox === true) {
      return "";
    } else {
      return "Por favor, verifique se você é um humano!";
    }
  };

  /*=============================================================================================================
..............................................reCAPTCHA v2......................................................
===============================================================================================================*/

  recaptchaLoaded = () => {
    console.log("Captcha carregou com sucesso!");
  };

  verifyReCaptchaV2 = (response) => {
    if (response) {
      this.setState({
        ...this.state,
        checked: true,
      });
    } else {
      this.setState({
        ...this.state,
        checked: false,
      });
    }
  };

  checkReCaptchaV2 = () => {
    if (this.state.checked === false) {
      return true;
    }
  };

  validationReCaptchaV2 = () => {
    const reCAPTCHA = this.state.checked;
    if (reCAPTCHA === true) {
      return "";
    } else {
      return "Por favor, verifique se você é um humano!";
    }
  };

  /*=============================================================================================================
.........................................SUBMETER QUESTIONÁRIO..................................................
===============================================================================================================*/

  createUser = () => {
    fetch("https://ka-users-api.herokuapp.com/users", {
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body: JSON.stringify({
        user: {
          name: this.state.name.value + " " + this.state.lastname.value,
          user: this.state.username.value,
          image_url:
            "http://www.ecp.org.br/wp-content/uploads/2017/12/default-avatar.png",
          email: this.state.email.value,
          password: this.state.password.value,
          password_confirmation: this.state.confirmPassword.value,
          address: this.state.state.myState,
          about: this.state.about.value,
        },
      }),
    })
      .then((response) => {
        if (response.status === 422) {
          this.setState({
            ...this.state,
            email: {
              ...this.state.email,
              error: "true",
              helperText: "Nome de usuário ou e-mail já cadastrado",
            },
            username: {
              ...this.state.username,
              error: "true",
              helperText: "Nome de usuário ou e-mail já cadastrado",
            },
          });
          throw Error;
        } else if (response.status >= 200 && response.status < 300) {
          return response.json();
        }
      })

      .then((response) => {
        this.setState({ ...this.state, newUserCreated: true });
      });
  };

  submeter = () => {
    const nameError = this.validationName();
    const lastnameError = this.validationLastname();
    const usernameError = this.validationUsername();
    const emailError = this.validationEmail();
    const passwordError = this.validationPassword();
    const confirmPasswordError = this.validationConfirmPassword();
    const aboutError = this.validationAbout();
    const stateError = this.validationState();
    const checkboxError = this.validationCheckbox();
    const captchaError = this.validationReCaptchaV2();

    this.setState({
      ...this.state,

      name: {
        value: this.state.name.value,
        error: nameError === "" ? "" : "true",
        helperText: nameError,
      },
      lastname: {
        value: this.state.lastname.value,
        error: lastnameError === "" ? "" : "true",
        helperText: lastnameError,
      },
      username: {
        value: this.state.username.value,
        error: usernameError === "" ? "" : "true",
        helperText: usernameError,
      },
      email: {
        value: this.state.email.value,
        error: emailError === "" ? "" : "true",
        helperText: emailError,
      },
      password: {
        value: this.state.password.value,
        error: passwordError === "" ? "" : "true",
        helperText: passwordError,
      },
      confirmPassword: {
        value: this.state.confirmPassword.value,
        error: confirmPasswordError === "" ? "" : "true",
        helperText: confirmPasswordError,
      },
      state: {
        value: this.state.state.value,
        error: stateError === "" ? "" : "true",
        helperText: stateError,
      },
      about: {
        value: this.state.about.value,
        error: aboutError === "" ? "" : "true",
        helperText: aboutError,
      },
    });

    if (
      nameError === "" &&
      lastnameError === "" &&
      usernameError === "" &&
      emailError === "" &&
      passwordError === "" &&
      confirmPasswordError === "" &&
      stateError === "" &&
      aboutError === "" &&
      checkboxError === "" &&
      captchaError === ""
    ) {
      this.createUser();
    }
  };

  /*=============================================================================================================
  ..............................................................................................................
.................................................RENDER.......................................................
.............................................................................................................
===========================================================================================================*/

  render() {
    const { classes } = this.props;
    if (this.state.newUserCreated === false) {
      return (
        <>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <img
                className="background"
                id="backgroundDefault "
                src={Background}
                alt="background"
                width="1225"
              />
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <div className="background">
                <div className="side">
                  <div id="bookbook">BookBook</div>
                  <div id="bookbook2">O seu novo clube de leitura</div>
                </div>

                <br />

                <div className="side2">
                  <div id="signup">
                    <div id="title">Crie uma nova conta</div>
                    <br />
                    <Container maxWidth="sm" className="box">
                      <InputLabel>
                        Compartilhe com seus amigos os comentários dos seus
                        livros favoritos!
                      </InputLabel>
                      <br />
                      <br />
                      <TextField
                        error={this.state.name.error}
                        label="Nome:"
                        onChange={this.setValueName}
                        value={this.state.name.value}
                        helperText={this.state.name.helperText}
                        variant="outlined"
                        fullWidth="true"
                        required="true"
                        size="small"
                      />
                      <br />
                      <br />
                      <TextField
                        error={this.state.lastname.error}
                        label="Sobrenome:"
                        onChange={this.setValueLastname}
                        value={this.state.lastname.value}
                        helperText={this.state.lastname.helperText}
                        variant="outlined"
                        fullWidth="true"
                        required="true"
                        size="small"
                      />
                      <br />
                      <br />
                      <TextField
                        error={this.state.username.error}
                        label="Nome de usuário:"
                        onChange={this.setValueUsername}
                        value={this.state.username.value}
                        helperText={this.state.username.helperText}
                        variant="outlined"
                        fullWidth="true"
                        required="true"
                        size="small"
                      />
                      <br />
                      <br />
                      <TextField
                        error={this.state.email.error}
                        label="E-mail:"
                        onChange={this.setValueEmail}
                        onBlur={this.formatEmail}
                        value={this.state.email.value}
                        helperText={this.state.email.helperText}
                        variant="outlined"
                        fullWidth="true"
                        required="true"
                        size="small"
                      />
                      <br />
                      <br />
                      <TextField
                        error={this.state.password.error}
                        label="Senha:"
                        onChange={this.setValuePassword}
                        value={this.state.password.value}
                        helperText={this.state.password.helperText}
                        variant="outlined"
                        fullWidth="true"
                        required="true"
                        type="password"
                        autoComplete="current-password"
                        size="small"
                      />
                      <br />
                      <br />
                      <TextField
                        error={this.state.confirmPassword.error}
                        label="Introduza novamente a senha:"
                        onChange={this.setValueConfirmPassword}
                        value={this.state.confirmPassword.value}
                        helperText={this.state.confirmPassword.helperText}
                        variant="outlined"
                        fullWidth="true"
                        required="true"
                        type="password"
                        autoComplete="current-password"
                        size="small"
                      />
                      <br />
                      <br />
                      <Autocomplete
                        size="small"
                        options={countries}
                        onChange={this.setValueState}
                        value={this.state.state.value}
                        getOptionLabel={(option) => option.title}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label="Estado (campo obrigatório)*"
                            variant="outlined"
                          />
                        )}
                      />
                      <br />
                      <br />
                      <TextField
                        error={this.state.about.error}
                        required="true"
                        label="Sobre você:"
                        multiline
                        rows={2}
                        onChange={this.setValueAbout}
                        value={this.state.about.value}
                        helperText={this.state.about.helperText}
                        variant="outlined"
                        fullWidth="true"
                        size="small"
                      />
                      <Container className="box">
                        <br />
                        <br />
                        <FormControl
                          required
                          error={this.checkCheckbox()}
                          component="fieldset"
                        >
                          <FormControlLabel
                            control={
                              <Checkbox
                                checked={this.state.checkbox}
                                onChange={this.setValueCheckbox}
                                onBlur={this.checkCheckbox}
                                name="checkedA"
                                color="primary"
                              />
                            }
                            label="Ao inscrever-se, você concorda com os nossos Termos de Serviço e a nossa Política de Privacidade."
                          />
                          <FormHelperText id="formHelperCheckbox">
                            Para prosseguir, você deve concordar com nossos
                            termos
                          </FormHelperText>
                          <br />
                        </FormControl>
                        <FormControl
                          required
                          error={this.checkReCaptchaV2()}
                          component="fieldset"
                        >
                          <Recaptcha
                            sitekey="6Le7WKcZAAAAAGMsAFdyvewlMOtuJTmAFkJTCWRh"
                            render="explicit"
                            hl="pt-BR"
                            onloadCallback={this.recaptchaLoaded}
                            verifyCallback={this.verifyReCaptchaV2}
                          />
                          <FormHelperText id="formHelperCheckbox">
                            Por favor, verifique se você é um humano!
                          </FormHelperText>
                        </FormControl>
                        <br />
                        <br />
                        <Button
                          className={classes.signUpButton}
                          type="submit"
                          onClick={() => this.submeter()}
                          variant="contained"
                          color="primary"
                        >
                          Cadastrar
                        </Button>
                        <br />
                        <br />
                      </Container>
                    </Container>
                  </div>
                </div>
              </div>
            </Paper>
          </Grid>
        </>
      );
    } else {
      return (
        <>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <img
                className="background"
                id="backgroundDefault "
                src={Background}
                alt="background"
                width="1225"
              />
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <div className="background">
                <div className="side">
                  <div id="bookbook">BookBook</div>
                  <div id="bookbook2">O seu novo clube de leitura</div>
                </div>

                <br />

                <div className="side3">
                  <div id="signup">
                    <div id="title">Bem-vindo(a), {this.state.name.value}!</div>
                    <br />
                    <Container maxWidth="sm" className="box">
                      <InputLabel>
                        Faça login para usufruir de todas as nossas
                        funcionalidades!
                      </InputLabel>
                    </Container>
                  </div>
                </div>
              </div>
            </Paper>
          </Grid>
        </>
      );
    }
  }
}

export default withRouter(withStyles(useStyles)(SignUp));
