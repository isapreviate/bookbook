import React from "react";
import { withStyles, createStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import { red } from "@material-ui/core/colors";
import EditBookReview from "../EditBookReview.js/EditBookReview";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import removeButton from "../buttons/removeButton";
import loadBooksList from "../helpers/loadBooksList";
import { addBooks } from "../../action";
import Rates from "../Rates/Rates";
import RateText from "../RateText/RateText";

const styles = ({ transitions }) =>
  createStyles({
    root: { minWidth: "595px", maxWidth: "595px" },
    cards: {
      margin: "0 auto",
    },
    superior: {
      width: "100%",
      height: "4px",
      backgroundColor: "rgba(18,29,77,1)",
      borderRadius: "5px",
    },
    h1: {
      color: "#3f51b5",
      fontFamily: "Monda",
      textTransform: "uppercase",
      fontSize: "16px",
      marginTop: "10px",
      marginBottom: "0px",
    },
    h2: {
      color: "#374242",
      fontFamily: "Monda",
      fontSize: "15px",
      marginTop: "0px",
      marginBottom: "0px",
    },
    h3: {
      width: "100%",
      minWidth: "595px",
      maxWidth: "595px",
      margin: "0 auto",
      background:
        "linear-gradient(180deg, rgba(18,29,77,1) 0%, rgba(0,0,0,1) 100%)",
      borderRadius: "4px",
      color: "white",
      fontFamily: "Monda",
      fontSize: "18px",
    },
    h4: {
      color: "indianred",
      fontFamily: "Monda",
      fontSize: "18px",
      marginBottom: "0px",
      marginTop: "10px",
    },
    button: {
      backgroundColor: "#antiquewhite",
      color: "darkslateblue",
      marginBottom: "16px",
      marginTop: "16px",
      marginLeft: "4px",
      marginRight: "4px",
      borderColor: "darkslateblue",
    },
    button2: {
      backgroundColor: "white",
      color: "crimson",
      marginBottom: "16px",
      marginTop: "16px",
      marginLeft: "4px",
      marginRight: "4px",
      borderColor: "indianred",
    },
    media: {
      margin: "0 auto",
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: transitions.create("transform", {
        duration: transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    avatar: {
      backgroundColor: red[500],
    },
    rating: {
      margin: "0 auto",
      width: "50%",
      fontFamily: "Monda",
      fontSize: "17px",
      marginTop: "0px",
    },
    title: {
      marginTop: "-20px",
      fontFamily: "Monda",
      fontSize: "10px",
    },
    welcome: {
      fontFamily: "Monda",
      fontSize: "14px",
      color: "gray",
    },
  });

class ReadBooks extends React.Component {
  state = {
    isEditing: null,
  };

  editReview = (bookId) => {
    this.setState({ isEditing: bookId });
  };

  cancelEdit = (e) => {
    e.preventDefault();
    this.setState({ isEditing: null });
  };

  removeBook = (bookId) => {
    removeButton(this.props.user.id, bookId, this.props.currentToken).then(() =>
      loadBooksList(this.props.user.id, this.props.currentToken)
        .then((res) => res.json())
        .then((res) => {
          this.props.addBooks(res);
        })
    );
  };

  render() {
    const { classes } = this.props;
    const { isEditing } = this.state;

    if (this.props.books.length > 0) {
      return (
        <>
          <CardMedia className={classes.h3}>Seus comentários</CardMedia>
          {this.props.books.map((book, key) => (
            <>
              <Paper elevation={7} key={key} className={classes.root}>
                <div className={classes.superior}></div>
                <h1 className={classes.h1}>{book.title}</h1>
                <h2 className={classes.h2}>{book.author}</h2>
                <Container>
                  <hr />
                  <CardMedia
                    className={classes.media}
                    image={book.image_url}
                    title={book.title}
                    style={{ height: 250, width: 166 }}
                  />
                  <hr />
                </Container>
                <div>
                  <container>
                    <div className={classes.rating}>
                      <p className={classes.h4}>Avalie: </p>
                      <Rates grade={book.grade} id={book.id} />
                      <CardHeader
                        className={classes.title}
                        subheader={<RateText grade={book.grade} />}
                      />
                    </div>
                  </container>
                  <Container>
                    <i>
                      <EditBookReview
                        review={book.review}
                        isEditing={isEditing}
                        editReview={this.editReview}
                        id={book.id}
                        loadBooks={this.loadBooks}
                        cancelEdit={this.cancelEdit}
                      />
                    </i>
                  </Container>
                  <br />
                </div>
                <Container>
                  <Button
                    className={classes.button}
                    size="small"
                    variant="outlined"
                    color="primary"
                    onClick={() => this.editReview(book.id)}
                  >
                    Avaliar
                  </Button>
                  <Button
                    className={classes.button2}
                    size="small"
                    variant="outlined"
                    color="secondary"
                    onClick={() => this.removeBook(book.id)}
                  >
                    Remover
                  </Button>
                  <br />
                </Container>
              </Paper>
              <br />
              <br />
            </>
          ))}
          <br />
        </>
      );
    } else {
      return (
        <>
          <CardMedia className={classes.h3}>Suas Críticas</CardMedia>
          <Paper elevation={20} className={classes.root}>
            <br />
            <Container>
              <Container>
                <hr className={classes.welcome} />
                <Container>
                  <p className={classes.welcome}>
                    Adicione livros a sua lista de leituras recentes e clique em
                    "concluído" para escrever suas críticas!
                  </p>
                </Container>
                <hr className={classes.welcome} />
                <br />
              </Container>
            </Container>
          </Paper>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
  books: state.bookshelf.books.filter((book) => book.shelf === 3),
});

const mapDispatchToProps = (dispatch) => ({
  addBooks: (books) => dispatch(addBooks(books)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ReadBooks));
